#define AppName 'Northwind App'
#define AppDir 'Northwind'
#define BuildDir 'src\Northwind.ConsoleApp\bin\Debug'
#define AppExeName 'Northwind.ConsoleApp.exe'
#define AppVendorName 'KR Software'
#define AppCopyright 'Copyright � 2016. ' + AppVendorName
#define AppURL 'http://coding4ever.net/'
#define AppVersion GetFileVersion('src\Northwind.ConsoleApp\bin\Debug\' + AppExeName)

[Setup]
AppName={#AppName}
AppVerName={#AppName} Versi {#AppVersion}
AppPublisher={#AppVendorName}
AppVersion={#AppVersion}
AllowCancelDuringInstall = yes
DefaultDirName={pf}\{#AppVendorName}\{#AppDir}
DefaultGroupName={#AppVendorName}
Compression = lzma
SolidCompression = yes
OutputDir=.
OutputBaseFilename=Setup-v{#AppVersion}
AllowNoIcons = yes
AlwaysRestart = no
AlwaysShowComponentsList = no
DisableProgramGroupPage = yes
AppendDefaultDirName = yes
CreateUninstallRegKey = yes
DisableStartupPrompt = yes
LanguageDetectionMethod=none
ShowLanguageDialog=no
UsePreviousLanguage=no
Uninstallable = yes
UninstallFilesDir={app}\uninst
UninstallDisplayIcon={app}\{#AppExeName},0
UninstallDisplayName={#AppName}
WindowVisible = no
AppCopyright={#AppCopyright}
FlatComponentsList = yes
PrivilegesRequired = admin
VersionInfoVersion={#AppVersion}

[Tasks]
Name: desktopicon; Description: {cm:CreateDesktopIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: checkedonce
Name: quicklaunchicon; Description: {cm:CreateQuickLaunchIcon}; GroupDescription: {cm:AdditionalIcons}; Flags: unchecked

[Files]
;My Application Runtime
Source: {#BuildDir}\*.config; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\*.exe; DestDir: {app}; Flags: ignoreversion
Source: {#BuildDir}\*.dll; DestDir: {app}; Flags: ignoreversion

[Icons]
Name: {group}\{#AppName}; Filename: {app}\{#AppExeName}; WorkingDir: {app}
Name: {userdesktop}\{#AppName}; Filename: {app}\{#AppExeName}; WorkingDir: {app}; Tasks: desktopicon

[Code]
procedure AboutButtonOnClick(Sender: TObject);
begin
  MsgBox('{#AppName}' #13#13 '{#AppCopyright}' #13 '{#AppURL}', mbInformation, mb_Ok);
end;

procedure CreateAboutButtonAndURLLabel(ParentForm: TSetupForm; CancelButton: TNewButton);
var
  AboutButton: TNewButton;
begin
  AboutButton := TNewButton.Create(ParentForm);
  AboutButton.Left := ParentForm.ClientWidth - CancelButton.Left - CancelButton.Width;
  AboutButton.Top := CancelButton.Top;
  AboutButton.Width := CancelButton.Width;
  AboutButton.Height := CancelButton.Height;
  AboutButton.Caption := '&About...';
  AboutButton.OnClick := @AboutButtonOnClick;
  AboutButton.Parent := ParentForm;  
end;

procedure InitializeWizard();
begin
  CreateAboutButtonAndURLLabel(WizardForm, WizardForm.CancelButton);  
end;